FROM mcr.microsoft.com/dotnet/sdk:6.0

WORKDIR /app

# see: https://reportgenerator.io/
RUN dotnet tool install --global dotnet-reportgenerator-globaltool --version 5.2.1

# .NET tooling installs global tools in the /root/.dotnet/tools directory
ENV PATH="${PATH}:/root/.dotnet/tools"

# Setup the entry point to use the ReportGenerator tool
ENTRYPOINT ["reportgenerator"]
